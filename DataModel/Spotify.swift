//
//  Spotify.swift
//  Spotifier
//
//  Created by Aleksandar Vacić on 5.6.17..
//  Copyright © 2017. Radiant Tap. All rights reserved.
//

import Foundation

final class Spotify {

	static let shared = Spotify()
	private init() {
	}

}

typealias JSON = [String: Any]

enum SpotifyError: Error {
	case invalidResponse
	case noData
	case invalidJSON
	case networkError(urlError: URLError?)
}


extension Spotify {
	enum SearchType: String {
		case artist
		case album
		case track
		case playlist
	}

	enum Path {
		case search(term: String, type: SearchType)
//		case artists(id: String)


		private var method: Spotify.Method {
			switch self {
			case .search:
				return .GET
			}
		}

		private var headers: [String: String] {
			return Spotify.commonHeaders
		}

		private var fullURL: URL {
			var path = ""

			switch self {
			case .search:
				path = "search"
//			case .artists(let id, let type):
//				path = "artists/\(id)/\(type)"
			}

			return baseURL.appendingPathComponent(path)
		}

		private var params: [String: String] {
			var p : [String: String] = [:]

			switch self {
			case .search(let term, let type):
				p["q"] = term
				p["type"] = type.rawValue
			}

			return p
		}

		private var encodedParams: String {
			switch self {
			case .search:
				return queryEncoded(params: params)
			}
		}

		private func queryEncoded(params: [String: String]) -> String {
			if params.count == 0 { return "" }

			var arr = [String]()
			for (key, value) in params {
				let s = "\(key)=\(value)"
				arr.append(s)
			}

			return arr.joined(separator: "&")
		}

		private func jsonEncoded(params: [String: String]) -> Data? {
			return try? JSONSerialization.data(withJSONObject: params)
		}

		fileprivate var urlRequest: URLRequest {
			guard var components = URLComponents(url: fullURL, resolvingAgainstBaseURL: false) else { fatalError("Invalid URL") }

			switch method {
			case .GET:
				components.query = queryEncoded(params: params)
			default:
				break
			}

			guard let url = components.url else { fatalError("Invalid URL") }
			var r = URLRequest(url: url)
			r.httpMethod = method.rawValue
			r.allHTTPHeaderFields = headers

			switch method {
			case .POST:
				r.httpBody = jsonEncoded(params: params)
				break
			default:
				break
			}

			return r
		}
	}
}


extension Spotify {
	typealias Callback = ( JSON?, SpotifyError? ) -> Void

	func call(path: Path, callback: @escaping Callback) {

		URLSession.shared.dataTask(with: path.urlRequest) {
			data, urlResponse, error in

			//	process the returned stuff, now
			if let error = error as? URLError {
				//	TODO: check error type
				callback(nil, SpotifyError.networkError(urlError: error))
				return
			}

			guard let httpURLResponse = urlResponse as? HTTPURLResponse else {
				callback(nil, SpotifyError.invalidResponse)
				return
			}

			if httpURLResponse.statusCode != 200 {
				callback(nil, SpotifyError.invalidResponse)
				return
			}

			guard let data = data else {
				callback(nil, SpotifyError.noData)
				return
			}

			guard
				let obj = try? JSONSerialization.jsonObject(with: data),
				let json = obj as? JSON
				else {
					callback(nil, SpotifyError.invalidJSON)
					return
			}

			callback(json, nil)
		}
	}
}

fileprivate extension Spotify {

	static let baseURL : URL = {
		guard let url = URL(string: "https://api.spotify.com/v1/") else { fatalError("Can't create base URL!") }
		return url
	}()

	static let commonHeaders: [String: String] = {
		return [
			"User-Agent": "Spotifier/1.0",
			"Accept": "application/json",
			"Accept-Charset": "utf-8",
			"Accept-Encoding": "gzip, deflate"
		]
	}()

	enum Method: String {
		case GET, POST, PUT, DELETE, HEAD
	}


}


