//
//  DataManager.swift
//  Spotifier
//
//  Created by Aleksandar Vacić on 5.6.17..
//  Copyright © 2017. Radiant Tap. All rights reserved.
//

import Foundation
import RTCoreDataStack

enum DataError: Error {
}

final class DataManager {

	let spotifyManager: Spotify
	let coreDataStack: RTCoreDataStack

	init(coreDataStack: RTCoreDataStack, spotifyManager: Spotify) {
		self.coreDataStack = coreDataStack
		self.spotifyManager = spotifyManager
	}
}


extension DataManager {
	typealias Callback = (Int, DataError?) -> Void

	func search(for term: String, type: Spotify.SearchType, callback: @escaping Callback) {

		let path = Spotify.Path.search(term: term, type: type)

		spotifyManager.call(path: path) {
			json, error in

		}
	}
}




