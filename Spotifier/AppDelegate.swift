//
//  AppDelegate.swift
//  Spotifier
//
//  Created by Admin on 5/29/17.
//  Copyright © 2017 nemaKmeTuznaPrica. All rights reserved.

import UIKit
import RTCoreDataStack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var coreDataStack: RTCoreDataStack?
    
    var spotifyManager: Spotify?
    var dataManager: DataManager?
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        spotifyManager = Spotify.shared
        
        coreDataStack = RTCoreDataStack {
            [unowned self] in
            
            self.configureManagers()
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}


extension AppDelegate {
    func configureManagers() {
        guard let cds = coreDataStack, let sm = spotifyManager else {
            fatalError()
        }
        
        dataManager = DataManager(coreDataStack: cds, spotifyManager: sm)
    }
}

